#include <stdio.h>
#include <stdlib.h>


int main(void) {
	
	//SOBRE ARQUIVOS TXT
	

	
	
	return 0;
}

int inserindo_dados_no_txt(){
	
		//Anexar dados no arquivo txt
	
	//File comando para criar um ponteiro de data. (*) � o ponteiro.
	FILE *file;
	
	//variaevel chama a funcao que aponta para um arquivo, possui dois argumentos. 1- caminho 2- comando(Read, Write, Alter).
	// A barra do caminho necessita ser tratada, duplicando - a. 
	// ERRADO : file = fopen("C:\Users\Lavinia de Queiroz\Documents\Lavs\Projects\primeiro_arquivo.txt","");
	// Certo:
	file= fopen ("C:\\Users\\Lavinia de Queiroz\\Documents\\Lavs\\Projects\\primeiro_arquivo.txt","a");
	
	
	//Condicao para, caso o ponteiro aponte para o nada exibir mensagem ao usuario.
	if(file == NULL){
		printf("Nao foi possivel abrir o arquivo.\n");
		//system("pause") impede que a promp feche.
		system("pause");
		//encerrar processo
		return(0);
	}
	
	//Imprime no arquivo, indicando que e ha uma nova insercao.
	fprintf(file,"Nova insercao:\n");
	
	//cria a variavel onde sera armazenada a frase a ser guardada.
	char frase[100];
	
	//Pega e guarda a sequencia de caracteres.
	printf("Digite o nome a ser inserido:\n");
	scanf("%s",&frase);
	
	//Comando "fputs" insere novos dados ao arquivo. Dois argumentos 1- Variavel que contem o valor a ser imprimido 2- Ponteiro do arquivo.
	fputs(frase,file);

	//fecha o arquivo
	fclose(file);
	
}

int lendo_mensagem_no_txt(){
	
	//Lendo o arquivo txt.
	
	//File comando para criar um ponteiro de data. (*) � o ponteiro.
	FILE *file;
	
	//variaevel chama a funcao que aponta para um arquivo, possui dois argumentos. 1- caminho 2- comando(Read, Write, Alter).
	// A barra do caminho necessita ser tratada, duplicando - a. 
	// ERRADO : file = fopen("C:\Users\Lavinia de Queiroz\Documents\Lavs\Projects\primeiro_arquivo.txt","");
	// Certo:
	file= fopen ("C:\\Users\\Lavinia de Queiroz\\Documents\\Lavs\\Projects\\primeiro_arquivo.txt","r");
	
	
	//Condicao para, caso o ponteiro aponte para o nada exibir mensagem ao usuario.
	if(file == NULL){
		printf("Nao foi possivel abrir o arquivo.\n");
		//system("pause") impede que a promp feche.
		system("pause");
		//encerrar processo
		return(0);
	}
	
	//cria string
	// para "char" deve sempre declarar a limitacao de caracteres entre chaves []
	char frase [100];
	
	//Controle de arquivo vazio. *
	//While - funcao que faz o que significa.
	//Deve conter uma condicao.
	//A condicao e "fgets", para pegar os dados do arquivo.
	//A funcao "fgets" necessita de dois argumentos. 1- variavel onde serao armazenados os dados 2- limitacao de caracteres 3- ponteiro do arquivo
	// A condicao � "Enquanto" o conteudo do arquivo for diferente de vazio entao exibir.
	while (fgets(frase, 100, file) !=NULL){
		
			//%s para ler uma cadeira de caracteres, seguido da variavel onde foram armazenados os dados do arquivo
		   printf("%s", frase);
	}
	//fecha o arquivo
	fclose(file);
	
	
}

int imprindo_mensagem_no_txt(){

	
	//File comando para criar um ponteiro de data. (*) � o ponteiro.
	FILE *file;
	
	//variaevel chama a funcao que aponta para um arquivo, possui dois argumentos. 1- caminho 2- comando(Read, Write, Alter).
	// A barra do caminho necessita ser tratada, duplicando - a. 
	// ERRADO : file = fopen("C:\Users\Lavinia de Queiroz\Documents\Lavs\Projects\primeiro_arquivo.txt","");
	// Certo:
	file = fopen("C:\\Users\\Lavinia de Queiroz\\Documents\\Lavs\\Projects\\primeiro_arquivo.txt","w");
	
	//Funcao fprintf imprime no arquivo, f de file. Dois arugumentos 1- ponteiro do arquivo 2- mensagem em "".
	fprintf(file, "Primeira mensagem gravada 2");
	
	//Funcao para fechar o arquivo com seguranca.
	fclose(file);

	

}
